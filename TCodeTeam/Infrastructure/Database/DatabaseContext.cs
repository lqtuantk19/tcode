﻿using Microsoft.EntityFrameworkCore;
using TCodeTeam.Models.Entities;

namespace TCodeTeam.Infrastructure.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Activity> Activity { get; set; }
        public DbSet<CodingActivity> CodingActivity { get; set; }
        public DbSet<ReadingActivity> ReadingActivity { get; set; }
        public DbSet<SectionActivity> SectionActivity { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<CommentVote> CommentVote { get; set; }
        public DbSet<Course> Course { get; set; }
        public DbSet<ItemTag> ItemTag { get; set; }
        public DbSet<Post> Post { get; set; }
        public DbSet<Section> Section { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<Vote> Vote { get; set; }
        public DatabaseContext(DbContextOptions options) : base(options)
        {

        }
    }
}
