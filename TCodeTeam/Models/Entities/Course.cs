﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("Course")]
    public class Course
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [StringLength(255, MinimumLength = 4, ErrorMessage = "Name should be between 4 and 255 characters in length")]
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        [Required(ErrorMessage = "Title is required")]
        [StringLength(255, MinimumLength = 4, ErrorMessage = "Title should be between 4 and 255 characters in length")]
        public string Title { get; set; }
        public string MetaTitle { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public int ViewCount { get; set; }
        public Guid AuthorId { get; set; }
        public Guid UpdatedBy { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}
