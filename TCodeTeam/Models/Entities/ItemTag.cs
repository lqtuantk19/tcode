﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("ItemTag")]
    public class ItemTag
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public Guid TagId { get; set; }
    }
}
