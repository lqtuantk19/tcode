﻿using System.ComponentModel.DataAnnotations;

namespace TCodeTeam.Models.Entities
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string NormalizedTitle { get; set; }
        public string MetaTitle { get; set; }
        public string Slug { get; set; }
        public string Content { get; set; }
    }
}
