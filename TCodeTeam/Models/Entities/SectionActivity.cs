﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("SectionActivity")]
    public class SectionActivity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid SectionId { get; set; }
        public Guid ActivityId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}
