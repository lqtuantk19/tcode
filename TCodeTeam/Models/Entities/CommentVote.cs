﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("CommentVote")]
    public class CommentVote
    {
        public Guid Id { get; set; }
        public bool IsUpvote { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
}
