﻿using System.ComponentModel.DataAnnotations;

namespace TCodeTeam.Models.Entities
{
    public class CodingActivity
    {
        [Key]
        public Guid Id { get; set; }
        public CodingActivityType Type { get; set; }
        public int Point { get; set; }
        public int MemoryLimit { get; set; }
        public double MaxExecutionTime { get; set; }
        public string Solution { get; set; }
        public string Tutorial { get; set; }
        public List<string> LanguageSupport { get; set; }
        public List<string> Input { get; set; }
        public List<string> Output { get; set; }
        public virtual Activity Activity { get; set; }
    }
    public enum CodingActivityType
    {
        Easy,
        Medium,
        Hard,
        VeryHard
    }
}
