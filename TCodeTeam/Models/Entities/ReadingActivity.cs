﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("ReadingActivity")]
    public class ReadingActivity
    {
        public Guid Id { get; set; }
        public int TimeToRead { get; set; }
        public virtual Activity Activity { get; set; }
    }
}
