﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TCodeTeam.Models.Entities
{
    [Table("Activity")]
    public class Activity
    {
        [Key]
        public Guid Id { get; set; }
        public Guid SectionId { get; set; }
        public Guid CourseId { get; set; }
        public ActivityType Type;
        [Required(ErrorMessage = "Name is required")]
        [StringLength(255, MinimumLength = 4, ErrorMessage = "Name should be between 4 and 255 characters in length")]
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        [Required(ErrorMessage = "Title is required")]
        [StringLength(255, MinimumLength = 4, ErrorMessage = "Title should be between 4 and 255 characters in length")]
        public string Title { get; set; }
        public string MetaTitle { get; set; }
        public string Summary { get; set; }
        public string ContentHTML { get; set; }
        public string Description { get; set; }
        public string PublishedAt { get; set; }
        public int ViewCount { get; set; }
        public Guid AuthorId { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime DeletedAt { get; set; }
    }
    public enum ActivityType
    {
        READING,
        VIDEO,
        QUIZ,
        CODING,
        OOP,
        SQL
    }
}
